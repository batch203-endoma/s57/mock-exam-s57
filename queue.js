let collection = [];

// Write the queue functions below.
function print() {
  return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  for (let i = 1; i <= collection.length; i++) {
    collection[i-1] = collection[i];
    collection.length--;
    return collection
  }
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty(){
  return (collection==0)
}
// Export create queue functions below.
module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};
